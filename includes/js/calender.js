        // Minified version of isMobile included in the HTML since it's small
        ! function (a) {
            var b = /iPhone/i,
                c = /iPod/i,
                d = /iPad/i,
                e = /(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,
                f = /Android/i,
                g = /(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i,
                h = /(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i,
                i = /IEMobile/i,
                j = /(?=.*\bWindows\b)(?=.*\bARM\b)/i,
                k = /BlackBerry/i,
                l = /BB10/i,
                m = /Opera Mini/i,
                n = /(CriOS|Chrome)(?=.*\bMobile\b)/i,
                o = /(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,
                p = new RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)", "i"),
                q = function (a, b) {
                    return a.test(b)
                },
                r = function (a) {
                    var r = a || navigator.userAgent,
                        s = r.split("[FBAN");
                    return "undefined" != typeof s[1] && (r = s[0]), s = r.split("Twitter"), "undefined" != typeof s[1] && (r = s[0]), this.apple = {
                        phone: q(b, r),
                        ipod: q(c, r),
                        tablet: !q(b, r) && q(d, r),
                        device: q(b, r) || q(c, r) || q(d, r)
                    }, this.amazon = {
                        phone: q(g, r),
                        tablet: !q(g, r) && q(h, r),
                        device: q(g, r) || q(h, r)
                    }, this.android = {
                        phone: q(g, r) || q(e, r),
                        tablet: !q(g, r) && !q(e, r) && (q(h, r) || q(f, r)),
                        device: q(g, r) || q(h, r) || q(e, r) || q(f, r)
                    }, this.windows = {
                        phone: q(i, r),
                        tablet: q(j, r),
                        device: q(i, r) || q(j, r)
                    }, this.other = {
                        blackberry: q(k, r),
                        blackberry10: q(l, r),
                        opera: q(m, r),
                        firefox: q(o, r),
                        chrome: q(n, r),
                        device: q(k, r) || q(l, r) || q(m, r) || q(o, r) || q(n, r)
                    }, this.seven_inch = q(p, r), this.any = this.apple.device || this.android.device || this.windows.device || this.other.device || this.seven_inch, this.phone = this.apple.phone || this.android.phone || this.windows.phone, this.tablet = this.apple.tablet || this.android.tablet || this.windows.tablet, "undefined" == typeof window ? this : void 0
                },
                s = function () {
                    var a = new r;
                    return a.Class = r, a
                };
            "undefined" != typeof module && module.exports && "undefined" == typeof window ? module.exports = r : "undefined" != typeof module && module.exports && "undefined" != typeof window ? module.exports = s() : "function" == typeof define && define.amd ? define("isMobile", [], a.isMobile = s()) : a.isMobile = s()
        }(this);


        // My own arbitrary use of isMobile, as an example
        (function () {

            // I only want to redirect iPhones, Android phones and a handful of 7" devices
            if (isMobile.apple.phone || isMobile.android.phone || isMobile.seven_inch) {

                //MOBILE

                //Calender

                var gNull = 0;

                jQuery(document).ready(function () {

                    var events = [];

                    jQuery("#from_date").caleran({
                        target: jQuery("#mobile_date"),
                        format: 'DD-MM-YYYY',
                        calendarCount: 1,
                        minDate: moment().utcOffset(7),
                        showFooter: false,
                        startDate: moment().utcOffset(7),
                        endDate: moment().utcOffset(7).add(1, 'days'),
                        onbeforeshow: function (caleran) {
                            gNull = 0;
                        },
                        ondraw: function (caleran) {
                            jQuery('.caleran-apply').removeClass("caleran-apply").addClass("caleran-apply-d");
                        },
                        onfirstselect: function (caleran, startDate) {
                            if (!jQuery('#keteranganHari')[0]) {
                                jQuery('.caleran-calendar').after('<div id="keteranganHari"></div>');
                            }

                            thisDay = startDate.unix()
                            jQuery('.caleran-day-unclick').removeClass('caleran-day-unclick').addClass("caleran-day");
                            jQuery('.caleran-day[data-value="' + thisDay + '"]').replaceWith("<div class='caleran-day-unclick' data-value='" + thisDay + "' title='" + events[thisDay] + "'><span>" + startDate.format('D') + "</span></i>");
                            caleran.config.maxDate = startDate.clone().add(30, "days");
                            caleran.config.minDate = moment().utcOffset(7);

                            var keteranganHari = jQuery('.caleran-day-unclick[data-value="' + thisDay + '"]').attr('title');
                            if (keteranganHari != 'undefined') {
                                jQuery('#keteranganHari').html(moment.unix(thisDay).format("DD-MM-YYYY") + ': ' + keteranganHari);
                            } else {
                                jQuery('#keteranganHari').remove();
                            }

                        },
                        onbeforeselect: function (caleran, start, end) {
                            if (end == null && start != null) {
                                gNull = 0;
                                caleran.config.startDate = start.clone();
                                caleran.config.endDate = start.clone().add(1, "days");
                                jQuery('#from_date').val(start.clone().format('DD-MM-YYYY'));
                                jQuery('#to_date').val(start.clone().add(1, "days").format('DD-MM-YYYY'));
                                jQuery('#from_date_day').text(start.clone().format('dddd'));
                                jQuery('#to_date_day').text(start.clone().add(1, "days").format('dddd'));
                                return true;
                            }
                            gNull = 1;
                            return true;
                        },
                        onafterselect: function (caleran, startDate, endDate) {
                            var diffD = endDate.diff(startDate, 'days');
                            jQuery('#from_date').val(startDate.clone().format('DD-MM-YYYY'));
                            jQuery('#to_date').val(endDate.clone().format('DD-MM-YYYY'));
                            jQuery('#from_date_day').text(startDate.clone().format('dddd'));
                            jQuery('#to_date_day').text(endDate.clone().format('dddd'));
                            // jQuery('#t_night').val(diffD).trigger('change.select2');
                            jQuery('#t_night').val(diffD);
                            caleran.config.minDate = moment().utcOffset(7);
                            caleran.config.maxDate = null;
                        },
                        onafterhide: function (caleran) {
                            if (gNull == 0) {
                                caleran.config.startDate = moment(jQuery('#from_date').val(), 'DD-MM-YYYY');
                                caleran.config.endDate = moment(jQuery('#to_date').val(), 'DD-MM-YYYY');
                            }
                        },
                        ondraw: function (caleran) {
                            jQuery.ajax({
                                type: "GET",
                                url: "https://jogjahotels.id/get-all-events",
                                crossDomain: true,
                                data: {
                                    id: ''
                                },
                                success: function (data) {

                                    jQuery.each(data, function (index, value) {
                                        jQuery('.caleran-day[data-value="' + index + '"]').css('color', 'red');
                                        jQuery('.caleran-day[data-value="' + index + '"]').prop('title', value);

                                        events[index] = value;
                                    });


                                },

                                error: function (XMLHttpRequest) {
                                    console.log(XMLHttpRequest.responseText);
                                }
                            })

                        },
                    });

                    jQuery("#from_date_day").on("click", function () {
                        var caleran = jQuery("#from_date").data("caleran");
                        caleran.showDropdown();
                    });
                    jQuery('#to_date').click(function () {
                        gNull = 0;
                        jQuery('.caleran-input').css({
                            "display": "flex"
                        });
                    });
                    jQuery('#to_date_day').click(function () {
                        gNull = 0;
                        jQuery('.caleran-input').css({
                            "display": "flex"
                        });
                    });
                })


                jQuery('#t_night').change(function (e) {
                    e.stopPropagation();
                    upbNM();
                });

                function upbNM() {
                    var diff = parseInt(jQuery('#t_night').val());
                    var caleran = jQuery("#from_date").data("caleran");
                    var to_date = moment(jQuery("#from_date").val(), "DD-MM-YYYY").add(diff, 'days');
                    caleran.config.endDate = to_date;
                    caleran.reDrawCalendars();

                    jQuery('#to_date').val(to_date.format('DD-MM-YYYY'));
                    jQuery('#to_date_day').text(to_date.format('dddd'))
                };

                /// button
                /// 
                /// 
                //mobile
                jQuery(document).ready(function () {
                    jQuery('.box-number.one .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 9) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.one .qtyminus').val("").removeAttr('style');
                                if (parseInt(jQuery('input[name=adults]').val()) < parseInt(jQuery('input[name=' + fieldName + ']').val())) {

                                    if (parseInt(jQuery('input[name=adults]').val()) < 36) {

                                        jQuery('.box-number.two .qtyminus').val("").removeAttr('style');
                                    } else {
                                        jQuery('.box-number.two .qtyplus').val("").css('color', '#aaa');
                                        jQuery('.box-number.two .qtyplus').val("").css('cursor', 'not-allowed');
                                    }
                                }
                            } else {
                                jQuery('.box-number.one .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.one .qtyplus').val("").css('cursor', 'not-allowed');
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                        }
                    });

                    jQuery('.box-number.two .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 36) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.two .qtyminus').val("").removeAttr('style');
                            } else {
                                jQuery('.box-number.two .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.two .qtyplus').val("").css('cursor', 'not-allowed');
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                        }
                    });

                    jQuery('.box-number.three .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 36) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.three .qtyminus').val("").removeAttr('style');
                            } else {
                                jQuery('.box-number.three .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.three .qtyplus').val("").css('cursor', 'not-allowed');
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                        }
                    });

                    jQuery('.box-number.night .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 30) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.three .qtyminus').val("").removeAttr('style');
                            } else {
                                jQuery('.box-number.three .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.three .qtyplus').val("").css('cursor', 'not-allowed');
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                        }
                        upbNM();
                    });

                    jQuery('.box-number.one .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 1) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.one .qtyplus').val("").removeAttr('style');
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                            jQuery('.box-number.one .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.one .qtyminus').val("").css('cursor', 'not-allowed');
                        }
                    });

                    jQuery('.box-number.two .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 1) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.two .qtyplus').val("").removeAttr('style');

                            if (parseInt(jQuery('input[name=rooms]').val()) > parseInt(jQuery('input[name=' + fieldName + ']').val())) {

                                if (parseInt(jQuery('input[name=rooms]').val()) > 1) {
                                    jQuery('.box-number.one .qtyplus').val("").removeAttr('style');
                                } else {
                                    jQuery('.box-number.one .qtyminus').val("").css('color', '#aaa');
                                    jQuery('.box-number.one .qtyminus').val("").css('cursor', 'not-allowed');
                                }

                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                            jQuery('.box-number.two .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.two .qtyminus').val("").css('cursor', 'not-allowed');
                        }
                    });

                    jQuery('.box-number.three .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 0) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.three .qtyplus').val("").removeAttr('style');
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(0);
                            jQuery('.box-number.three .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.three .qtyminus').val("").css('cursor', 'not-allowed');
                        }
                    });

                    jQuery('.box-number.night .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 1) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.three .qtyplus').val("").removeAttr('style');
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                            jQuery('.box-number.three .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.three .qtyminus').val("").css('cursor', 'not-allowed');
                        }
                        upbNM();
                    });
                });



            } else {


                //DESKTOP
                jQuery(document).ready(function () {
                    jQuery('#from_date').caleran({
                        minDate: moment().utcOffset(7),
                        format: 'DD-MM-YYYY',
                        calendarCount: 1,
                        singleDate: true,
                        showHeader: false,
                        showFooter: false,
                        onafterselect: function (caleran, startDate, endDate) {
                            var diff = parseInt(jQuery('.t_night').val());
                            jQuery("#to_date").off();
                            jQuery('#to_date').caleran({
                                minDate: startDate.clone().add(1, 'days'),
                                maxDate: startDate.clone().add(30, 'days'),
                                format: 'DD-MM-YYYY',
                                calendarCount: 1,
                                showHeader: false,
                                showFooter: false,
                                singleDate: true,
                                onafterselect: function (tcaleran, tstartDate, tendDate) {
                                    jQuery('#to_date_day').text(tstartDate.clone().format('dddd'));
                                    upN();
                                    jQuery('.caleran-container.caleran-popup').css('display', 'none');
                                    // tcaleran.hideDropdown();
                                },
                            });

                            var tcaleran = jQuery("#to_date").data("caleran");
                            tcaleran.config.startDate = startDate.clone().add(diff, 'days');
                            jQuery('#from_date_day').text(startDate.clone().format('dddd'));
                            jQuery('#to_date_day').text(startDate.clone().add(diff, 'days').format('dddd'));
                            tcaleran.reDrawCalendars();
                            upN();
                            // caleran.hideDropdown();
                            jQuery('.caleran-container.caleran-popup').css('display', 'none');
                        },
                        ondraw: function (caleran) {
                            jQuery.ajax({
                                type: "GET",
                                url: "https://jogjahotels.id/get-all-events",
                                crossDomain: true,
                                data: {
                                    id: ''
                                },
                                success: function (data) {

                                    jQuery.each(data, function (index, value) {
                                        jQuery('.caleran-day[data-value="' + index + '"]').css('color', 'red');
                                        jQuery('.caleran-day[data-value="' + index + '"]').prop('title', value);
                                    });


                                },

                                error: function (XMLHttpRequest) {
                                    console.log(XMLHttpRequest.responseText);
                                }
                            })

                        },
                    });

                    jQuery('#to_date').caleran({
                        minDate: moment(jQuery("#from_date").val(), "DD-MM-YYYY").add(1, 'days'),
                        maxDate: moment(jQuery("#from_date").val(), "DD-MM-YYYY").add(30, 'days'),
                        format: 'DD-MM-YYYY',
                        calendarCount: 1,
                        showHeader: false,
                        showFooter: false,
                        singleDate: true,
                        onafterselect: function (caleran, startDate, endDate) {
                            var duration;
                            var tcaleran = jQuery("#from_date").data("caleran");
                            var from = tcaleran.config.startDate;
                            duration = startDate.diff(from, 'days');
                            jQuery('#to_date_day').text(startDate.clone().format('dddd'));
                            upN();
                            // caleran.hideDropdown();
                            jQuery('.caleran-container.caleran-popup').css('display', 'none');
                        },
                        ondraw: function (caleran) {
                            jQuery.ajax({
                                type: "GET",
                                url: "https://jogjahotels.id/get-all-events",
                                crossDomain: true,
                                data: {
                                    id: ''
                                },
                                success: function (data) {

                                    jQuery.each(data, function (index, value) {
                                        jQuery('.caleran-day[data-value="' + index + '"]').css('color', 'red');
                                        jQuery('.caleran-day[data-value="' + index + '"]').prop('title', value);
                                    });


                                },

                                error: function (XMLHttpRequest) {
                                    console.log(XMLHttpRequest.responseText);
                                }
                            })

                        },
                    });
                });

                // function upN(){jQuery('.t_night').val(dateDiffInDays(getJsDate('#from_date'),getJsDate('#to_date'))).trigger('change.select2');}
                function upN() {
                    jQuery('.t_night').val(dateDiffInDays(getJsDate('#from_date'), getJsDate('#to_date')));
                }
                jQuery("#from_date_day").on("click", function () {
                    var caleran = jQuery("#from_date").data("caleran");
                    caleran.showDropdown();
                });
                jQuery("#to_date_day").on("click", function () {
                    var caleran = jQuery("#to_date").data("caleran");
                    caleran.showDropdown();
                });
                jQuery('.t_night').change(function (e) {
                    e.stopPropagation();
                    upbN();
                });

                function upbN() {
                    var diff = parseInt(jQuery('.t_night').val());
                    var tcaleran = jQuery("#to_date").data("caleran");
                    var from_date = moment(jQuery("#from_date").val(), "DD-MM-YYYY");
                    tcaleran.config.startDate = from_date.add(diff, 'days'),
                        tcaleran.reDrawCalendars();
                    jQuery('#to_date_day').text(tcaleran.config.startDate.clone().format('dddd'));
                    upN();
                }

                function getJsDate(s) {
                    var f = jQuery(s).val().split("-");
                    var mD = new Date(f[2], f[1] - 1, f[0]);
                    return mD;
                }

                var _MS_PER_DAY = 1000 * 60 * 60 * 24;

                function dateDiffInDays(a, b) {
                    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
                    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

                    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
                }


                ///button
                jQuery(document).ready(function () {
                    jQuery('.box-number.one .qtyplus').off();
                    jQuery('.box-number.one .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 9) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.one .qtyminus').val("").removeAttr('style');
                                if (parseInt(jQuery('input[name=adults]').val()) < parseInt(jQuery('input[name=' + fieldName + ']').val())) {

                                    if (parseInt(jQuery('input[name=adults]').val()) < 36) {
                                        jQuery('.box-number.two .qtyminus').val("").removeAttr('style');
                                    } else {
                                        jQuery('.box-number.two .qtyplus').val("").css('color', '#aaa');
                                        jQuery('.box-number.two .qtyplus').val("").css('cursor', 'not-allowed');
                                    }
                                }
                            } else {
                                jQuery('.box-number.one .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.one .qtyplus').val("").css('cursor', 'not-allowed');
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                        }
                    });

                    jQuery('.box-number.two .qtyplus').off();
                    jQuery('.box-number.two .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 36) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.two .qtyminus').val("").removeAttr('style');
                            } else {
                                jQuery('.box-number.two .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.two .qtyplus').val("").css('cursor', 'not-allowed');
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                        }
                    });

                    jQuery('.box-number.three .qtyplus').off();
                    jQuery('.box-number.three .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 36) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.three .qtyminus').val("").removeAttr('style');
                            } else {
                                jQuery('.box-number.three .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.three .qtyplus').val("").css('cursor', 'not-allowed');
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(0);
                        }
                    });

                    jQuery('.box-number.one .qtyminus').off();
                    jQuery('.box-number.one .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 1) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.one .qtyplus').val("").removeAttr('style');
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                            jQuery('.box-number.one .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.one .qtyminus').val("").css('cursor', 'not-allowed');
                        }
                    });

                    jQuery('.box-number.two .qtyminus').off();
                    jQuery('.box-number.two .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 1) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.two .qtyplus').val("").removeAttr('style');

                            if (parseInt(jQuery('input[name=rooms]').val()) > parseInt(jQuery('input[name=' + fieldName + ']').val())) {

                                if (parseInt(jQuery('input[name=rooms]').val()) > 1) {
                                    jQuery('.box-number.one .qtyplus').val("").removeAttr('style');
                                } else {
                                    jQuery('.box-number.one .qtyminus').val("").css('color', '#aaa');
                                    jQuery('.box-number.one .qtyminus').val("").css('cursor', 'not-allowed');
                                }

                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                            jQuery('.box-number.two .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.two .qtyminus').val("").css('cursor', 'not-allowed');
                        }
                    });

                    jQuery('.box-number.three .qtyminus').off();
                    jQuery('.box-number.three .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 0) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.three .qtyplus').val("").removeAttr('style');
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(0);
                            jQuery('.box-number.three .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.three .qtyminus').val("").css('cursor', 'not-allowed');
                        }
                    });


                    jQuery('.box-number.night .qtyplus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal)) {
                            if (currentVal < 30) {
                                jQuery('input[name=' + fieldName + ']').val(currentVal + 1);
                                jQuery('.box-number.night .qtyminus').val("").removeAttr('style');
                                upbN();
                            } else {
                                jQuery('.box-number.night .qtyplus').val("").css('color', '#aaa');
                                jQuery('.box-number.night .qtyplus').val("").css('cursor', 'not-allowed');
                                upbN();
                            }
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                            upbNM();
                        }
                    });

                    jQuery('.box-number.night .qtyminus').click(function (e) {
                        e.preventDefault();
                        fieldName = jQuery(this).attr('name');
                        var currentVal = parseInt(jQuery('input[name=' + fieldName + ']').val());
                        if (!isNaN(currentVal) && currentVal > 1) {
                            jQuery('input[name=' + fieldName + ']').val(currentVal - 1);
                            jQuery('.box-number.night .qtyplus').val("").removeAttr('style');
                            upbN();
                        } else {
                            jQuery('input[name=' + fieldName + ']').val(1);
                            jQuery('.box-number.night .qtyminus').val("").css('color', '#aaa');
                            jQuery('.box-number.night .qtyminus').val("").css('cursor', 'not-allowed');
                            upbN();
                        }
                    });
                });

            } //end else

        })();