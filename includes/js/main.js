  jQuery(document).ready(function (jQuery) {

    // hidden arrow in slider, if only one image
    var numItems = jQuery('.item').length;
    if (numItems < 2) {
      jQuery('a.carousel-control').css('display', 'none');
    }

    jQuery('#homeSliderCarousel').carousel({
      interval: 4000
    });

    jQuery('#carousel-text').html(jQuery('#slide-content-0').html());

    //Handles the carousel thumbnails
    jQuery('[id^=carousel-selector-]').click(function () {
      var id_selector = jQuery(this).attr("id");
      var id = id_selector.substr(id_selector.length - 1);
      var id = parseInt(id);
      jQuery('#homeSliderCarousel').carousel(id);
    });


    // When the carousel slides, auto update the text
    jQuery('#homeSliderCarousel').on('slid', function (e) {
      var id = jQuery('.item.active').data('slide-number');
      jQuery('#carousel-text').html(jQuery('#slide-content-' + id).html());
    });

    /* Navbar Dropdown Menu */
    // jQuery('.navbar .dropdown').hover(function () {
    //   jQuery(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
    // }, function () {
    //   jQuery(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
    // });


  });


  jQuery(document).ready(function () {
    jQuery('#image-popups').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function (item) {
          return item.el.attr('title') + '<small></small>';
        }
      }
    });
    jQuery('.thumb-deals, body.meeting-room .page-content').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function (item) {
          return item.el.attr('title') + '<small></small>';
        }
      }
    });
  });

  // jQuery(document).ready(function () {
  //   jQuery('article').magnificPopup({
  //     delegate: 'a',
  //     type: 'image',
  //     tLoading: 'Loading image #%curr%...',
  //     mainClass: 'mfp-img-mobile',
  //     gallery: {
  //       enabled: true,
  //       navigateByImgClick: true,
  //       preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
  //     },
  //     image: {
  //       tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
  //       titleSrc: function (item) {
  //         return item.el.attr('title') + '<small></small>';
  //       }
  //     }
  //   });
  // });

	jQuery(document).ready(function(){
		/* Testimonial */
		jQuery('.home-text-slide').owlCarousel({
			loop:false,
			margin:18,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			loop:true,
			nav:true,
			navText: ["<span class='glyphicon glyphicon-chevron-left'></span>","<span class='glyphicon glyphicon-chevron-right'>"],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					margin:5,
				},
				600:{
					items:1,
					margin:8,
				},
				1024:{
					items:1,
					margin:14,
				}
			}
		});
	});

  jQuery(document).ready(function () {
    jQuery('.gallery-view').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.gallery-nav'
    });
    jQuery('.gallery-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.gallery-view',
      dots: false,
      centerMode: true,
      focusOnSelect: true,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]

    });
    jQuery('.slider-nav').show();

  });


  /* Smooth scroll */

  jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 70) {
      jQuery('#mainNav').addClass('nav-background');
    } else {
      jQuery('#mainNav').removeClass('nav-background');
    }
  });
  /* Smooth scroll */
  jQuery(document).ready(function (jQuery) {
    jQuery("#btn-book").click(function () {
      jQuery('body').animate({
        scrollTop: jQuery("#booknow").offset().top
      }, 2000);
    });
    jQuery('#nav-menu-mobile').click(function () {
      jQuery('#navbar-hamburger').toggleClass('hidden');
      jQuery('#navbar-close').toggleClass('hidden');
    });
  });

  jQuery( ".box-toggle" ).click(function() {
    jQuery(".hiddenroom").slideToggle( "slow" );
    jQuery(".box-toggle").hide();

});
