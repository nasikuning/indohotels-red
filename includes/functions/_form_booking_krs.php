<?php 
function form_booking() { ?>
<div class="mid-booking">
	<h2>INSTANT BOOKING</h2>
	<form action="//www.indohotels.id/referrer/booking/" method="GET" role="form" onSubmit="return validateBook()">
		<div class="col-xs-4 field">
			<label for="start_date">Check-in</label>
			<span readonly='true' id="from_date_day"></span>
			<input readonly='true' id="from_date" type="text" class="from_date from_date input-sm form-control" name="start" value=""
			/>
			<input readonly='true' id="mobile_date" style="display:none" type="text" />
		</div>
		<div class="col-xs-4 field">
			<label for="">Total Night(s)</label>
			<span class="bookcheck"></span>
				<div class="box-number night">
					<input type="button" value="" class="qtyminus" field="night" />
					<input type="text" id="t_night" readonly="true" name="night" class="t_night qty" value="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
					/>
					<input type="button" value="" class="qtyplus" field="night" />
				</div>
		</div>
		<div class="col-xs-4 field">
			<label for="">Check-out</label>
			<span readonly='true' id="to_date_day"></span>
			<input readonly='true' type="text" id="to_date" class="to_date to_date_mobile input-sm form-control" name="end" value=""
			/>
		</div>
		<!-- <input type="hidden" name="refferer" value="<?php //echo $_SERVER['SERVER_NAME']; ?>"> -->
		<input type="hidden" name="refferer" value="www.dutagardenhotel.com">

		<div class="clearfix"></div>

		<div class="form-field-row widget">
			<div class="form-group book-option">
				<label>Number of Room(s)</label>
				<div class="box-number one">
					<input type="button" value="" class="qtyminus" field="rooms" />
					<input type="text" readonly="true" name="rooms" value="1" class="qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
					/>
					<input type="button" value="" class="qtyplus" field="rooms" />
				</div>
			</div>
			<div class="form-group book-option">
				<label>Adult(s)/room</label>
				<div class="box-number two">
					<input type="button" value="" class="qtyminus" field="adults" />
					<input type="text" readonly="true" name="adults" value="2" class="qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
					/>
					<input type="button" value="" class="qtyplus" field="adults" />
				</div>
			</div>
			<div class="form-group book-option">
				<label>Child(ren)/room</label>
				<div class="box-number three">
					<input type="button" value="" class="qtyminus" field="children" />
					<input type="text" readonly="true" name="children" value="0" class="qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
					/>
					<input type="button" value="" class="qtyplus" field="children" />
				</div>
			</div>
		</div>
		<div class="total-book"></div>
		<div class="last-book"></div>
		<div class="col-xs-24 m_b_10 p_0">
			<button class="btn btn-primary btn-block" type="submit">Check Availability</button>
		</div>
	</form>
	<div class="powered-by">
		<img src="https://media.jogjahotels.id/website/powered-by-indohotels-with-bg.svg" width="90px">
	</div>
	<?php }