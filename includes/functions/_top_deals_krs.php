<?php function top_deals(){ ?>
<section class="deals">
	<div class="container">
			<?php
		$args = array('post_type' => 'deals');
		query_posts($args);
		if (have_posts()): ?>
		<h3><?php _e('Promotions', karisma_text_domain); ?></h3>
		<span class="line"></span>
		<div class="row">
		<?php while (have_posts()) : the_post(); ?>
			<div class="col-md-4">
				<div class="room-thumb thumbnail">
					<div id="post-<?php the_ID(); ?>" <?php post_class( 'deals-post'); ?>>
						<!-- post title -->

						<!-- post thumbnail -->
						<div class="thumb-deals">
							<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
							<a href="<?php the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail('large'); // Declare pixel size you need inside the array ?>
							</a>
							<?php endif; ?>
						</div>
						<!-- /post thumbnail -->
						<h4 class="title-room-list">
							<?php the_title(); ?>
						</h4>
						<!-- /post title -->
						<?php the_excerpt(); ?>
					</div>
				</div>
			</div>

			<?php endwhile; ?>
			<?php endif; ?>
		</div>

		<div class="clearfix"></div>
	</div>
</section>
<?php }

function footer_slide(){ ?>
<section class="hotel-info-home">
	<div class="container">
		<div class="home-award-title text-center">
			<h3 class="title text-center"><?php _e('Testimonials', karisma_text_domain); ?></h3>
			<span class="line"></span>
		</div>
		<div class="home-carousel owl-carousel home-text-slide">
			<?php
			$args = array(
				'post_type' => 'hotel-info',
				'category_name' => 'testimonial'
				);
			query_posts($args);
			if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="item thumb">
					<div class="slide-awards">
						<!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
						<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
						<?php endif; ?>
						<!-- /post thumbnail -->
					</div>
					<!-- post title -->
					<div class="title-slide">
						<h2>
							<?php echo get_the_title(); ?>
						</h2>
						<span><?php the_content(); ?></span>
					</div>
				</div>
				<!-- /post title -->
				<?php endwhile; ?>
				<?php endif; ?>
		</div>
	</div>
	<div class="clearfix"></div>
</section>
<?php }
