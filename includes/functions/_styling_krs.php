<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
if ( !function_exists('header_cover') ) :
	function krs_header_cover() {
		if ( has_post_thumbnail()) : // Check if thumbnail exists
		$coverimage = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "cover" );
		$background_header = 'style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(' .$coverimage[0] .');"';

		echo $background_header;
		endif;
	}
	endif;

	/* Coloring*/
	if ( !function_exists('krs_main_color') ) :
		function krs_main_color() {
			if ( function_exists( 'ot_get_option' ) ) :
				if ( (!empty(ot_get_option( 'krs_main_colorpicker') ) ) == true) : 

					$mainColor = ot_get_option( 'krs_main_colorpicker');
					$seColor = ot_get_option('krs_secound_colorpicker');
					$terColor = ot_get_option('krs_tertiary_colorpicker');
					$fontsColor = ot_get_option('krs_main_fonts');
					$fontHover = ot_get_option('krs_font_hover');
					//background
					$bg_welcome =  ot_get_option('krs_background_text1');
					$bg_room =  ot_get_option('krs_background_text1');
					$bg_section2 =  ot_get_option('krs_background_section2');
					$bg_section3 =  ot_get_option('krs_background_section3');

					require_once('_custom_css_krs.php');
				endif;
			endif;
		}
		endif;

		function filter_ot_recognized_font_families( $array, $field_id ) {

			/* only run the filter when the field ID is my_google_fonts_headings */
			if ( $field_id == 'my_google_fonts_headings' ) {
			  $array = array(
				'sans-serif'    => 'sans-serif',
				'open-sans'     => '"Open Sans", sans-serif',
				'droid-sans'    => '"Droid Sans", sans-serif'
			  );
			}

			return $array;

		  }
