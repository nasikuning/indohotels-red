<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/*
* Add logo with dynamic metatag - main page
* add_action( 'do_krs_logo', 'krs_headlogo' ); in init.php
*/
if ( !function_exists('krs_headlogo') ) :
function krs_headlogo() { 
	$get_logo_image = ot_get_option('krs_logo');
	if (ot_get_option('krs_logo_actived') == 'on') { 
		echo '<a href="' . home_url() . '"><img src="' . $get_logo_image . '" class="img-responsive logo-img" alt="' . get_bloginfo('name') . '" title="' . get_bloginfo('name') . '" /></a>';
		if( (is_single() || is_page() || is_archive() || is_search()) and !(is_front_page()) ) :
			echo '<a class="hidden" href="'. home_url() . '" title="' . get_bloginfo('name') . '">' . get_bloginfo('name') . '</a>';
		else : 
			echo '<h1 class="hidden"><a href="'. home_url() . '" title="' . get_bloginfo('name') . '">' . get_bloginfo('name') . '</a></h1>';
		endif;
		} else { 
		if( (is_single() || is_page() || is_archive()) and !(is_front_page()) ) :
		echo '<h3 class="logo-text"><a href="'. home_url() . '" title="' . get_bloginfo('name') . '">';
		if ( ot_get_option('krs_icon_name') != '' )  { echo '<span class="' . ot_get_option('krs_icon_name') . '"></span> '; }
		echo get_bloginfo('name') . '</a></h1>';
		else : 
		echo '<h1 class="logo-text"><a href="'. home_url() . '" title="' . get_bloginfo('name') . '">';
		if ( ot_get_option('krs_icon_name') != '' ) { echo '<span class="' . ot_get_option('krs_icon_name') . '"></span> '; }
		echo get_bloginfo('name') . '</a></h1>';
		endif;
		}	
	} 
endif;