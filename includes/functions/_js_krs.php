<?php

// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }


// Load karisma scripts (header.php)
function krs_loadjs()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {


        wp_register_script('bootstrap', krs_style . 'vendor/bootstrap/js/bootstrap.min.js', array(), false, true );
        wp_register_script( 'animated',krs_style . 'js/animated.js',array(), false, true );
        // wp_register_script( 'viewportchecker',krs_style . 'js/lib/viewportchecker.js',array(), false, true );
        wp_register_script( 'viewportchecker','https://cdnjs.cloudflare.com/ajax/libs/jQuery-viewport-checker/1.8.8/jquery.viewportchecker.min.js',array(), false, true );
        wp_register_script('main', krs_style . 'js/main.js', array(), false, true );
        wp_register_script('owl', krs_style . 'vendor/owl/owl.carousel.min.js', array(), false, true );
        wp_register_script('slick', krs_style . 'vendor/slick/slick.min.js', array(), false, true );
        wp_register_script('magnific', krs_style . 'vendor/magnific/jquery.magnific-popup.min.js', array(), false, false );
        wp_register_script('moment', krs_style . 'vendor/caleran/js/moment.min.js', array(), false, true );
        wp_register_script('caleran', krs_style . 'vendor/caleran/js/caleran.min.js', array(), false, true );
        wp_register_script('calender-full', 'https://media.jogjahotels.id/js/calender-full.js', array(), false, true );
        
        wp_enqueue_script('jquery');
        wp_enqueue_script('bootstrap'); // Enqueue it!
        wp_enqueue_script('viewportchecker'); // Enqueue it!
        wp_enqueue_script('owl');
        wp_enqueue_script('slick');
        wp_enqueue_script('magnific');
        wp_enqueue_script('moment' ); 
        wp_enqueue_script('caleran' ); 
        wp_enqueue_script('calender-full' ); 
        wp_enqueue_script('main' ); // load all custom javascript
        wp_enqueue_script('animated'); // Enqueue it!
        

            }
        }
