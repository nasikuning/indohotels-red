<?php
add_action('widgets_init', 'krs_info_load_widgets');

function krs_info_load_widgets()
{
	register_widget('Info_Widget');
}

/* ==  Widget ==============================*/

class Info_Widget extends WP_Widget {
	

/* ==  Widget Setup ==============================*/

	function __construct()
	{
		$widget_ops = array('classname' => 'krs_info_widget', 'description' => __('A widget that displays your Information like address and contact .', karisma_text_domain) );

		$control_ops = array('id_base' => 'krs_info_widget');

		parent::__construct('krs_info_widget', __('IDH: Hotel Information', karisma_text_domain), $widget_ops, $control_ops);
	}
	

/* ==  Display Widget ==============================*/

	function widget($args, $instance)
	{
		extract($args);
		
		$title = $instance['title'];
		$categories = $instance['categories'];
		$posts = $instance['posts'];
		
		echo $before_widget;
		?>

	<?php
		if($title) {
			echo $before_title.$title.$after_title;
		}
		?>


	<?php if(!empty(ot_get_option('krs_address'))) : ?>
	<div>
		<i class="fa fa-map-marker"></i>
		<?php echo ot_get_option('krs_address'); ?>
	</div>
	<?php endif; ?>

	<?php if(!empty(ot_get_option('krs_phone'))) : ?>
		<div>
			<i class="fa fa-phone"></i>
			<p>
				<?php echo ot_get_option('krs_phone'); ?>
			</p>
		</div>
		<?php endif; ?>
		<?php if(!empty(ot_get_option('krs_email'))) : ?>
		<div>
			<i class="fa fa-envelope"></i>
			<p>
				<a href="mailto:<?php echo ot_get_option('krs_email'); ?>">
					<?php echo ot_get_option('krs_email'); ?>
				</a>
			</p>
		</div>
		<?php endif; ?>

		<!-- END WIDGET -->
		<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['categories'] = $new_instance['categories'];
		$instance['posts'] = $new_instance['posts'];
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Address', 'categories' => 'all', 'posts' => 4);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">
					<?php _e('Title:', karisma_text_domain) ?>
				</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>"
				    value="<?php echo $instance['title']; ?>" />
			</p>
			<?php 
	}
}
?>