<?php

// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

// Load karisma styles
function krs_styles()
{
    wp_register_style('karisma-style', krs_url . 'style.css', array(), wp_get_theme()->Version, 'all');
    wp_register_style('bootstrap-min', krs_style . 'vendor/bootstrap/css/bootstrap.min.css', array(), '3.3.7', 'all');
    wp_enqueue_style('font-awesome-min', 'https://use.fontawesome.com/releases/v5.0.4/css/all.css', array(), '5.0.4','all' );
    wp_register_style('animate-css', krs_style . 'vendor/animate/css/animate.css', array(), '0.7.0', 'all');
    wp_register_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), '1.0', 'all');
    wp_register_style('poppins','https://fonts.googleapis.com/css?family=Lato:400,400i,700,900', array(), '1.0','all' );
    wp_register_style('raleway','//fonts.googleapis.com/css?family=Raleway:300,400,500,600', array(), '1.0','all' );
    wp_register_style('caleran', krs_style . 'vendor/caleran/css/caleran.min.css', array(), '1.2','all' );
    wp_register_style('owl', krs_style . 'vendor/owl/owl.carousel.min.css', array(), '1.2','all' );
    wp_register_style('owl-theme', krs_style . 'vendor/owl/owl.theme.default.min.css', array(), '1.2','all' );
    wp_register_style('slick', krs_style . 'vendor/slick/slick.css', array(), '1.2','all' );
    wp_register_style('slick-theme', krs_style . 'vendor/slick/slick-theme.css', array(), '1.2','all' );
    wp_register_style('magnific-popup', krs_style . 'vendor/magnific/magnific-popup.css', array(), '1.2','all' );
    wp_register_style('flaticon', krs_style . 'vendor/flaticon/flaticon.css', array(), '1.2','all' );

    wp_enqueue_style('jquery-ui'); // Enqueue bootstrap.min.css!
    wp_enqueue_style('bootstrap-min'); // Enqueue bootstrap.min.css!
    wp_enqueue_style('animate-css'); // Enqueue animate.css!
    wp_enqueue_style('font-awesome-min'); // Enqueue font-awesome.min.css!
    wp_enqueue_style('poppins' );
    wp_enqueue_style('raleway');
    wp_enqueue_style('owl' );
    wp_enqueue_style('owl-theme' );
    wp_enqueue_style('caleran');
    // if (is_page('gallery')) :
        wp_enqueue_style('slick');
        wp_enqueue_style('slick-theme');
    // endif;
    wp_enqueue_style('karisma-style'); // Enqueue Style.css!
    wp_enqueue_style('magnific-popup'); // Enqueue magnific-popup.css!
    wp_enqueue_style('flaticon');
    }
