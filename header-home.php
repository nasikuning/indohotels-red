<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>

	<script type="text/javascript">
		jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 70) {
				jQuery('#mainNav').addClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'block'});
			} else {
				jQuery('#mainNav').removeClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'none'});
			}
		});
	</script>

</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header id="homeSliderCarousel" class="carousel slide header-home clear" role="banner">
		<!-- Wrapper for Slides -->
		<!-- logo -->
		<div class="wrap-logo">
			<div class="logo">
				<?php krs_headlogo(); ?>
			</div>
		</div>
		<!-- /logo -->
		<div class="carousel-inner">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=0;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == 0) {
								$active = 'active';
							} else {$active = '';}
							$full_img_src = wp_get_attachment_image_src( $id, 'gallery-slide' );
							$thumb_img_src = wp_get_attachment_image_src( $id, 'gallery-slide' );
							?>
							<div class="item <?php echo $active; ?>">
								<div class="home-slider" style="background-image:url('<?php echo $thumb_img_src[0]; ?>');"></div>
							</div>
							<?php
						}
					}
				}
			}
			?>
		</div>
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=-1;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == -1) {
								$active = 'active';
							} else {$active = '';}
							?>
							<li data-target="#homeSliderCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>"></li>

						</div>
						<?php
					}
				}
			}
		}
		?>

	</ol>
	<!-- Controls -->
	<a class="left carousel-control" href="#homeSliderCarousel" data-slide="prev">
		<span class="icon-prev"></span>
	</a>
	<a class="right carousel-control" href="#homeSliderCarousel" data-slide="next">
		<span class="icon-next"></span>
	</a>

	<div class="shadow-box">
	</div>
	<!-- nav -->
	<nav id="mainNav" class="navbar navbar-custom">
		<div class="navbar-brand"><?php krs_headlogo(); ?></div>
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> MENU <i class="fa fa-bars"></i>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php karisma_nav(); ?>
			</div>
		</div>
	</nav>
	<!-- /nav -->

	<div class="booking-box">
			<?php do_shortcode("[booking_engine]")?>
		</div>
</header>
	<!-- /header -->
