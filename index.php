<?php get_header('image'); ?>
<div class="container">
<main role="main" class="blog col-md-8">
	<!-- section -->
	<section class="">

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
