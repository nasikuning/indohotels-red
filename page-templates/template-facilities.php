<?php
/* Template Name: Facilities Template */ get_header('image'); ?>

<main role="main" class="col-md-12">
	<div class="container text-center">
		<!-- container -->
		<!-- section -->
		<section>
			<?php
			$args = array(
				'post_type' => 'hotel-info',
				'category_name' => 'facilities',
			);
			query_posts($args);
			if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="box-container col-md-6">
				<div class="room-thumb thumbnail">
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'rooms-post'); ?>>
						<div class="thumb">
							<?php
							$images = rwmb_meta( 'indohotels_imgHotelInfo', 'size=gallery-slide' ); // Since 4.8.0
							if ( !empty( $images ) ) :
									$i=0;
									foreach ( $images as $image ) {
										if($i++ != 0) {
											$hidden = 'hidden';
										} else {
											$hidden = '';
										}
									echo '<a class="'.$hidden.'" href="'. $image['url'] .'" title="'.$image['title'].'">';
									echo '<img title="'.$image['title'].'" alt="'.$image['title'].'" src="'. $image['url'] .'">';
									echo '</a>'; 
									} 
							else : ?>
								<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
									<?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
								<?php endif; ?>
							<?php endif; ?>
						</div>
						<div class="box-text text-center">
							<h4><?php the_title(); ?></h4>
								<div class="room-info">
									<?php echo rwmb_meta('indohotels_room_balcony'); ?>
								</div>
								<?php the_excerpt(); ?>
						</div>
						<script>
							jQuery('.post-<?php the_ID(); ?> .thumb').magnificPopup({
								delegate: 'a',
								type: 'image',
								tLoading: 'Loading image #%curr%...',
								mainClass: 'mfp-img-mobile',
								gallery: {
									enabled: true,
									navigateByImgClick: true,
									preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
								},
								image: {
									tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
									titleSrc: function (item) {
									return item.el.attr('title') + '<small></small>';
									}
								}
							});
						</script>
					</article>
				</div>
			</div>

			<?php endwhile; ?>

			<?php else: ?>

			<!-- article -->
			<article>
				<h2>
					<?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
				</h2>
			</article>
			<!-- /article -->

			<?php endif; ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>
	<!-- end container -->
</main>

<?php get_footer(); ?>
