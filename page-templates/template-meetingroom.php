<?php /* Template Name: Meeting Template */ get_header('image'); ?>

<main role="main" class="col-md-12 ">
	<!-- section -->
	<section class="container">
		<?php
		$args = array('post_type'=>'meetings-events');
		query_posts($args);
		?>
		<h1 class="title text-center"><?php the_title(); ?></h1>
			<div class="box-gallery">
				<div id="image-popups" class="gallery-view">
					<!-- Slider -->
					<?php if (have_posts()):
					$i=0;
					while (have_posts()) : the_post();
					?>
					<div class="item <?php echo ($i==0)?'active':'' ?>" data-slide-number="<?php echo $i; ?>">
						<div class="slider-item">
							<a href="<?php the_post_thumbnail_url('gallery-slide'); ?>" title="<?php the_title_attribute(); ?>">
								<img class="image-popups" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
							</a>
						</div>
						<h4><?php the_title(); ?></h4>
					</div>
					<?php $i++; ?>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>


				<div class="gallery-nav">
					<!-- Slider -->
					<?php if (have_posts()):
						$i=0;
						while (have_posts()) : the_post();
					?>
					<div class="item <?php echo ($i==0)?'active':'' ?>" data-slide-number="<?php echo $i; ?>">
						<?php the_post_thumbnail('gallery-slide');?>
					</div>
					<?php $i++; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</div> <!-- end slider -->
		</div>

		<?php
		 if (have_posts()): while (have_posts()) : the_post();
		?>

		<div class="page-content">
			<?php the_content(); ?>
		</div>

		<?php endwhile; ?>
		<?php endif; ?>

</section>
<!-- /section -->
</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
