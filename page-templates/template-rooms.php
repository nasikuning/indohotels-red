<?php 
/* Template Name: Rooms Page Template */ get_header('image'); ?>

<main role="main" class="col-md-12">
	<div class="container text-center"> <!-- container -->
		<!-- section -->
			<h1 class="title text-center"><?php the_title(); ?></h1>
		<section class="row">
			<?php 
			$args = array('post_type'=>'rooms');
			query_posts($args);
			if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="col-md-6">
				<div class="room-thumb thumbnail">
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class('rooms-post row'); ?>>
						<!-- post title -->
						<h2 class="title-room-list">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						</h2>
						<!-- /post title -->
						<div class="room-info text-center">
							<?php echo rwmb_meta('indohotels_room_balcony'); ?>
						</div>
						<!-- post thumbnail -->
						<div class="thumb">
						<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail(array(300,150)); // Declare pixel size you need inside the array ?>
							</a>
						<?php endif; ?>
						</div>
						<!-- /post thumbnail -->
						<div class="room-price">
						</div>
						<button class="book-room btn-block"><a href="<?php the_permalink(); ?>">Room Details</a></button>

					</article>
					<!-- /article -->
				</div>
			</div>

		<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h2>
		</article>
		<!-- /article -->

	<?php endif; ?>

	<?php get_template_part('pagination'); ?>

</section>
<!-- /section -->
</div> <!-- end container -->
</main>

<?php get_footer(); ?>