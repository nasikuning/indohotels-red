<?php /* Template Name: Home Template */ get_header('home' ); ?>
<main role="main">
	<!-- section -->
	<section id="ros-in">
		<div class="container">
			<h2>
				<?php echo ot_get_option('krs_ros_in_title');?>
			</h2>
			<p>
				<?php echo ot_get_option('krs_ros_in');?>
			</p>
		</div>
	</section>
	<!-- /section -->
	<!-- gallery -->

	<?php top_deals(); ?>

	<?php if (ot_get_option('krs_room_actived') != 'no') : ?>
	<!-- section -->

	<section id="room-section" class="padtb-large">

		<div class="container">

			<?php
		$args = array(
			'post_type'=> ot_get_option('krs_section_2'),
        	'posts_per_page' => 6,
        );

		$krs_query = new WP_Query( $args );

        $count = $krs_query->post_count;

        if(($count == 2) || ($count == 4)) {
            $col = 'col-md-6';
        } else {
            $col = 'col-md-4';
        }

        if ($krs_query->have_posts()): ?>
				<div class="box-bg" <?php if($col=='col-md-4' ) { echo 'style="width: 70%; margin: 0 auto";'; } ?>>
					<h2>
						<?php echo ot_get_option('krs_headline_section2');?>
					</h2>
					<div class="row">
						<?php while ($krs_query->have_posts()) : $krs_query->the_post(); ?>
						<div class="<?php echo $col ?>">
							<div class="box-room">
								<div class="thumb">
									<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_post_thumbnail('gallery-facility-slide'); // Declare pixel size you need inside the array ?>
									</a>
									<?php endif; ?>
								</div>
								<h4>
									<?php the_title(); ?>
								</h4>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>

				</div>
				<!-- /box-bg -->
		</div>
	</section>
	<!-- /section -->
	<?php endif; ?>
	<?php if (ot_get_option('krs_section3_actived') != 'no') : ?>
	<section class="hotel-property text-center">
		<div class="container">
			<?php
		$args = array(
			'post_type' => 'hotel-info',
			'category_name' => 'property'
			);
		query_posts($args);
		if (have_posts()) : ?>
				<h3>
					<?php echo ot_get_option('krs_headline_section3');?>
				</h3>
				<span class="line"></span>
				<div class="row">
					<?php while (have_posts()) : the_post(); ?>
					<div class="item col-md-4">
						<div class="thumbnails">
							<?php if ( has_post_thumbnail() ) : ?>
							<a href="<?php the_post_thumbnail_url('gallery-slide'); ?>" title="<?php the_title_attribute(); ?>">
								<img class="image-popups" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
								<div class="overlay"></div>
							</a>
							<?php endif; ?>
						</div>
						<h4>
							<?php echo get_the_title(); ?>
						</h4>
					</div>
					<?php endwhile; ?>
					<?php endif; ?>
					<div class="clearfix"></div>
				</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if (ot_get_option('gallery-slideshow') != 'no') : ?>
	<section class="home-facilities">
		<?php
		$args = array(
			'post_type' => 'gallery',
			'phototype'  => 'home',
			);
		query_posts($args);
		if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="box-thumb">
				<div class="thumb col-sm-12 col-md-6">
					<div class="thumbnail thumbnail-gallery">
						<!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
						<?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
						<?php endif; ?>
						<!-- /post thumbnail -->
					</div>
					<!-- post title -->
					<div class="title-gallery-home">
					<div class="logo-gallery ">
					<?php
						$logos = rwmb_meta( 'indohotels_logoGalleryDetails', 'size=thumbnails' ); // Since 4.8.0
						if ( !empty( $logos ) ) {
							foreach ( $logos as $logo ) {
							$full_img_src = wp_get_attachment_image_src( $logo['ID'], 'large' );
							$thumb_img_src = wp_get_attachment_image_src( $logo['ID'], 'logo-menu-thumb' );
							echo '<img src="'.$thumb_img_src[0].'" src-data="'.$full_img_src[0].'" alt="'.get_the_title().'" class="img-responsive">';
							}
						}
					?>
					</div>
						<h2>
							<?php echo get_the_title(); ?>
						</h2>
						<div class="gallery-time">Opening hours :
							<span>
								<?php echo rwmb_meta('gallery_openning'); ?>
							</span> -
							<span>
								<?php echo rwmb_meta('gallery_closing'); ?>
							</span>
						</div class="gallery-telephone">
						<div>Telephone :
							<span>
								<?php echo rwmb_meta('gallery_telephone'); ?>
							</span>
						</div>

					</div>
				</div>
				<!-- /post title -->
			</div>
			</div>
			<?php endwhile; ?>
			<?php endif; ?>
	</section>
	<!-- end gallery -->
	<?php endif;?>
</main>
<?php get_footer(); ?>
