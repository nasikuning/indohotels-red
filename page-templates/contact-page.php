<?php /* Template Name: Contact Template */ get_header('image'); ?>


<main role="main" class="container-fluid">
	<div class="map-section">
		<?php if (!empty(rwmb_meta('near_location'))) : ?>
		<div class="col-md-6 contact-map">
			<h2>Our Location</h2>
			<?php echo rwmb_meta('map'); ?>
		</div>
		<div class="col-md-6 near-desc">
			<h2>Attractions</h2>
			<?php echo rwmb_meta('near_location'); ?>
		</div>
		<?php else: ?>
		<div class="col-md-12">
			<h2>Our Location</h2>
			<?php echo rwmb_meta('map'); ?>
		</div>
		<?php endif; ?>
	</div><!-- end .map-section -->
	<div class="clearfix"></div>

	<!-- section -->
	<section class="container-fluid">
		<h2 class="text-center">Contact Us</h2>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- <h1 class="title text-center"><?php //the_title(); ?></h1> -->

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<br class="clear">

				<div class="row">
					<div class="col-md-6 col-sm-6">
						<?php the_content(); ?>
					</div><!-- end .col-md-6 col-sm-6 -->
					<div class="col-md-6 col-sm-6">
						<?php /*
						<div class="contact-headline">
							<h3><?php echo rwmb_meta('contact_title'); ?></h3>
						</div>
						<div class="contact-address">
							<?php echo rwmb_meta('contact_address'); ?>
						</div>
						*/?>
						<div class="contact-info">
							<div class="info-text">
								<i class="fa fa-map-marker" title="Address"></i>
								<?php echo rwmb_meta('contact_address'); ?>
							</div>
							<div class="info-text">
								<i class="fa fa-phone" title="Phone"></i>
								<ul>
									<?php
									$values = rwmb_meta( 'contact_phone' );
									foreach ( $values as $value )
									{
										echo '<li>'. $value . '</li>';
									}
									?>
								</ul>
							</div>
							<div class="info-text">
								<i class="fa fa-mobile" title="Mobile"></i>
								<ul>
									<?php
									$values = rwmb_meta( 'contact_mobile' );
									foreach ( $values as $value )
									{
										echo '<li>'. $value . '</li>';
									}
									?>
								</ul>
							</div>
							<div class="info-text">
								<i class="fa fa-fax" title="Fax"></i>
								<ul>
									<?php
									$values = rwmb_meta( 'contact_fax' );
									foreach ( $values as $value )
									{
										echo '<li>'. $value . '</li>';
									}
									?>
								</ul>
							</div>
							<div class="info-text">
								<i class="fa fa-envelope" title="Email"></i>
								<ul>
									<?php
									$values = rwmb_meta( 'contact_email' );
									foreach ( $values as $value )
									{
										echo '<li>'. $value . '</li>';
									}
									?>
								</ul>
							</div>

						</div>
					</div><!-- end .col-md-6 col-sm-6 -->
				</div><!-- end .row -->


			</article>
			<!-- /article -->

		<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h2 class="title text-center"><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h2>

		</article>
		<!-- /article -->

	<?php endif; ?>

</section>

<!-- /section -->
</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
